package edu.ucsd.cs110w.temperature;

/**
 * 
 */

/**
 * TODO (cs110wau): write class javadoc
 *
 * @author cs110wau
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) 
	{
		super(t);
	}
	public String toString() 
	{ 
		return "" + this.getValue() + " K"; 
	} 
	@Override 
	public Temperature toCelsius() { 
		float val1 = (float) (this.getValue() - 273.15);
		return new Celsius(val1);
	} 
	@Override 
	public Temperature toFahrenheit() { 
		float val1 = (float)(this.getValue() - 273.15);
		val1 = (float) (val1 * 1.8);
		val1 = val1 + 32;
		return new Fahrenheit(val1);
	} 
	
	public Temperature toKelvin()
	{
		return new Kelvin (this.getValue());
	}


}
