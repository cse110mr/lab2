/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wau
 *
 */
public class Celsius extends Temperature
{
	public Celsius (float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		
		return new Celsius (this.getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		float val1 = this.getValue() * 9;
		val1 = val1/5;
		val1 = val1+32;
		return new Fahrenheit(val1);
	}
	
	public Temperature toKelvin()
	{
		float val1 = (float) (this.getValue() + 273.15);
		return new Kelvin(val1);
	}
}
