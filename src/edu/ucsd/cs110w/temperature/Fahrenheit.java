/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wau
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit (float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		float val1 = this.getValue() -32;
		val1 = val1*5;
		val1 = val1/9;
		return new Celsius(val1);
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit (this.getValue());
	}
	
	public Temperature toKelvin()
	{
		float val1 = (float)(this.getValue() - 32);
		val1 = (float) (val1 * (5/9));
		val1 = (float) (val1 + 273.15);
		return new Kelvin(val1);
	}

}
