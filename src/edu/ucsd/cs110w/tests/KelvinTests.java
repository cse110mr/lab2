/**
 * 
 */
package edu.ucsd.cs110w.tests;

import edu.ucsd.cs110w.temperature.Celsius;
import edu.ucsd.cs110w.temperature.Kelvin;
import edu.ucsd.cs110w.temperature.Temperature;
import junit.framework.TestCase;

/**
 * TODO (cs110wau): write class javadoc
 * 
 * @author cs110wau
 */
public class KelvinTests extends TestCase {

	private float delta = 0.001f;

	public void testKelvinToCelsius() {
		Kelvin temp = new Kelvin(0);
		Temperature convert = temp.toKelvin();
		assertEquals(0, convert.getValue(), delta);
	}

}
